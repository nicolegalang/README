---
layout: markdown_page
title: "Kevin Chu's Weekly Snippets"
---

# Quarterly Priorities:
This content is meant to communicate how I intend to allocate my time. It should remain consistent on the time-scales of quarters.

## FY Q1 2023
| Theme | Notes | Percent |
| ------ | ------ | ------ |
| Team strategy and alignment | Deployment alignments, investment cases, Feature Flag strategy, Focus on Execution | 20% |
| Research | Develop cross-stage, higher level research thesis and questions | 10% |
| Team Development | Performance Indicators, CDF Reviews, Career Development, GitLab Values Coaching, Promotions | 20% |
| Coverage | Cover my team when they're out, Monitor coverage starting in April 1 | 25% |
| Sensing Mechanisms | Deep dive into Octopus Deploy and Launch Darkly - CDF participation, Open Feature participation, Enterprise Buyer Focus | 20% |
| Personal Development | Writing, Reading, Learning | 5% |

## FY Q4 2022
| Theme | Notes | Percent |
| ------ | ------ | ------ |
| Team strategy and alignment | Deployment alignments, investment cases, strategy for things like feature flag, secrets management, cost management | 35% |
| GTM | Usecases, marketing materials, Product GTM alignment | 15% |
| Onboarding Release PM | Help Chris ramp up | 10% |
| Team Development | Performance Indicators, CDF Reviews, Career Development, GitLab Values Coaching, Promotions | 15% |
| Coverage | Cover my team when they're out, take initiatives so they can move quicker | 5% |
| Sensing Mechanisms | Continuous Interview, Competition| 10% |
| Personal Development | Writing, Reading, Learning | 10% |

# [Snippets](http://blog.idonethis.com/google-snippets-internal-tool/)

## 2022-03-07
* [ ] Review [CDF events SIG spec and use cases](https://github.com/cdfoundation/sig-events/blob/main/vocabulary-draft/README.md)
* [ ] Review and contribute to [Open-Feature spec](https://github.com/open-feature/spec)
* [ ] Continue to make progress on investment cases https://gitlab.com/gitlab-com/Product/-/issues/3915 and https://gitlab.com/gitlab-com/Product/-/issues/1634 
* [ ] Figure out how Ecosystem and Import is working with JiHu

## 2022-02-28
* [x] [Move Wayne Enterprises Demo](https://gitlab.com/gitlab-com/Product/-/issues/3823)
* [x] [Feature Flag Investment Refresh](https://gitlab.com/gitlab-com/Product/-/issues/1634)
* [x] Understand [automation proposals](https://gitlab.com/gitlab-org/gitlab/-/issues/242194#note_585970260)
* [x] Start Clickhouse Backend Investment Case
* [x] [Review Ops Section Direction](https://gitlab.com/gitlab-com/Product/-/issues/3792#note_842414124)

## 2022-02-14
* [x] [Monitor PI update](https://gitlab.com/internal-handbook/internal-handbook.gitlab.io/-/merge_requests/333)
* [x] [Document Kubernetes First](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/98766)
* [ ] [Review Ops Section Direction](https://gitlab.com/gitlab-com/Product/-/issues/3792#note_842414124)
* [ ] Feature Flag Investment Refresh
* [x] How does Feature Flag work today from an operational perspective?
* [x] Open Feature Flag

## 2022-02-07
* [x] Benefit of [stages refresh](https://gitlab.com/gitlab-com/Product/-/issues/3528)
* [x] Next steps on [CDF Events](https://gitlab.com/gitlab-com/Product/-/issues/3782)
* [x] Open Feature initial conversations
* [ ] Feature Flag Investment Refresh
* [ ] How does Feature Flag work today from an operational perspective?

## 2022-01-24
* [x] [Establish Feature Flag Vision](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/94503)
* [ ] Benefit of [stages refresh](https://gitlab.com/gitlab-com/Product/-/issues/3528)
* [x] Review [Delivery Automation](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/97283)
* [ ] Setup Gainsight to detect deployment
* [ ] Write [Blog post](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12859)

## 2022-01-10
* [x] Collaborate on proposal for [starting Deployment Management](https://gitlab.com/gitlab-com/Product/-/issues/3652)
* [ ] Setup some sensing mechanism (Chorus, Gainsight)
* [x] Draft Letter from the editor for Deployment
* [x] Setup CNCP interviews
* [ ] Write [Blog post](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12859)

## 2022-01-04
* [x] Create proposal for starting Deployment Management
* [ ] Setup some sensing mechanism (Chorus, Gainsight)
* [ ] Draft Letter from the editor for Deployment
* [ ] Blog post - The importance of being Precise?

## 2021-12-20
* [ ] Create first [state of Deployment](https://gitlab.com/gitlab-com/Product/-/issues/3571)
* [x] [How does Monitor contribute to Deployment?](https://gitlab.com/gitlab-com/Product/-/issues/3577)
* [ ] Setup some sensing mechanism (Chorus, Gainsight)
* [ ] Update Feature Flag Direction Page

## 2021-12-06
* [ ] Setup some sensing mechanism (Chorus, Gainsight)
* [ ] Update Feature Flag Direction Page
* [x] Follow-up with conversations about nont-gitlab deployment customers
* [x] Update pages based on Argus Announcement

## 2021-11-08
* [x] Project Argus work
* [ ] Delivery stage benefits
* [ ] [Deployment churn customers](https://gitlab.com/gitlab-com/Product/-/issues/3180)
* [x] [Investment Cases](https://gitlab.com/gitlab-com/Product/-/boards/2427050?&label_name%5B%5D=Investment%20Case&label_name%5B%5D=section%3A%3Aops)

## 2021-11-01
* [x] Prep Haven Life EBR
* [ ] [Deployment churn customers](https://gitlab.com/gitlab-com/Product/-/issues/3180)
* [x] Help proactively plan 14.6, 14.7
* [ ] Delivery stage benefits
* [ ] Dig into Feature Flags

## 2021-10-25
* [ ] Find customers that might be able to choose why [they went a different direction on deployment tools](https://gitlab.com/gitlab-com/Product/-/issues/3180)
* [x] [Deployment Investment Case](https://gitlab.com/gitlab-com/Product/-/issues/3281)
* [ ] Create skeleton plans for 14.5, 14.6, 14.7
* [x] Plan and Build boards for Release
* [x] [Secrets Management Investment Case](https://gitlab.com/gitlab-com/Product/-/issues/3283)

## 2021-10-18
* [x] Catch up from unexpected OOO
* [ ] Find customers that might be able to choose why [they went a different direction on deployment tools](https://gitlab.com/gitlab-com/Product/-/issues/3180)
* [ ] Create skeleton plans for 14.5, 14.6, 14.7
* [ ] Plan and Build boards for Release
* [ ] 10% Investments

## 2021-10-04
* [x] CAB Quarterly meeting
* [ ] Find customers that might be able to choose why [they went a different direction on deployment tools](https://gitlab.com/gitlab-com/Product/-/issues/3180)
* [ ] Create plans for 14.5, 14.6, 14.7
* [ ] Release Orchestration JTBD
* [ ] Plan and Build boards for Release
* [ ] 10% Investments

## 2021-09-27
* [x] Record product update for review
* [x] Prepare for team member 360
* [x] vn promotion doc
* [x] Find customers that might be able to choose why they went a different direction on deployment tools
* [x] Create plans for 14.5, 14.6, 14.7
* [ ] Release Orchestration JTBD

## 2021-09-20
* [x] Merge [JTBD](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/89516) for Environment Management
* [ ] Create high level plan to tackle Environments and Release Orchestration
* [x] Make [deployment slides](https://docs.google.com/presentation/d/1eJnc7pvsjcHm7UDH-XBDZYcnZuqX5rjGFiaZvl6yIFU/edit#slide=id.geec9814ee5_0_0) pretty
* [ ] Prepare for team member 360
* [ ] Release Orchestration JTBD
* [ ] vn promotion doc

## 2021-09-13
* [x] [CB onboarding](https://gitlab.com/gitlab-com/Product/-/issues/3050)
* [x] Release working session with T-Mobile
* [x] Decker's Communication for Influence Training (09-16 and 09-17)
* [ ] Merge [JTBD](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/89516) for Environment Management
* [x] Prep and Interview Product Ops
* [ ] Create high level plan to tackle Environments and Release Orchestration
* [ ] Release Orchestration JTBD
* [ ] Prepare for team member 360


## 2021-09-07
* [x] Welcome Chris to GitLab
* [x] [Release 14.4 Planning](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/82)
* [x] RPIs for 14.4
* [x] CAB Meeting Presentation
* [ ] Merge JTBD for Environment Management
* [ ] Release Orchestration JTBD
* [ ] Create high level plan to tackle Environments and Release Orchestration

## 2021-08-30
* [x] Create CB onboarding issues/plan
* [x] [Farnoosh coverage](https://gitlab.com/gitlab-com/Product/-/issues/3016)
* [x] 360 Review - self, direct reports, and peers
* [ ] Create high level plan to tackle Environments and Release Orchestration
* [x] [August direction updates](https://gitlab.com/gitlab-com/Product/-/issues/2992)
* [x] Organize [Investment Cases](https://gitlab.com/gitlab-com/Product/-/issues/3038)
* [ ] [Release 14.4 Planning](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/82)

## 2021-08-02
1. [Execute on deployment direction next steps](https://gitlab.com/kbychu-apps/deployment-support/-/issues/1)
1. [Release 14.3 plan](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/66)
1. Release Metrics and Data
1. 10% Investment Case

## 2021-07-26
1. Execute on deployment direction next steps
1. Add relevant infor for Release and Monitor to [Category Health](https://about.gitlab.com/handbook/product/category-health/)
1. Update Release direction pages and work with Monitor PM to update monitor direction pages - https://gitlab.com/gitlab-com/Product/-/issues/2878 
1. Take some personal time this week for mental and emotional health

## 2021-07-12
1. Welcome and work with [Monitor PM](https://gitlab.com/gitlab-com/Product/-/issues/2809) to ramp up
1. Hopefully wrap up [Hiring Release PM](https://gitlab.com/gl-talent-acquisition/req-intake/-/issues/1375)
1. [Release Group 14.2 plan](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/65)
1. Deployment Direction Next steps
1. Release Post Items

## 2021-06-21
1. [Hiring Release PM](https://gitlab.com/gl-talent-acquisition/req-intake/-/issues/1375)
1. Deployment direction/AMA/Category updates
1. Organize Existing Environment and Release Orchestration Issues and epics
1. [June Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/2722)

## 2021-06-14
1. [Hiring Release PM](https://gitlab.com/gl-talent-acquisition/req-intake/-/issues/1375)
1. [Hiring Monitor PM](https://gitlab.com/gl-recruiting/req-intake/-/issues/1359)
1. Experiment with [Deployment Diagrams](https://gitlab.com/gitlab-com/Product/-/issues/2684)
1. Organize Existing Environment and Release Orchestration Issues and epics
1. Prep Release Post Items and kickoff
1. June Direction UPdates

## 2021-05-24
1. [Hiring Release PM](https://gitlab.com/gl-talent-acquisition/req-intake/-/issues/1375)
1. [Hiring Monitor PM](https://gitlab.com/gl-recruiting/req-intake/-/issues/1359)
1. [Deployment based Optimization](https://gitlab.com/gitlab-com/Product/-/issues/2572)
1. [Multi opportunity assessment template](https://gitlab.com/gitlab-com/Product/-/issues/2306)
1. [May Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/2585)

## 2021-05-17
1. [Hiring Release PM](https://gitlab.com/gl-talent-acquisition/req-intake/-/issues/1375)
1. [Hiring Monitor PM](https://gitlab.com/gl-recruiting/req-intake/-/issues/1359)
1. Complete [ADO Design Sprint](https://gitlab.com/gitlab-org/gitlab/-/issues/330163)
1. Ensure all RPIs for Monitor/Release are complete
1. [Deployment based Optimization](https://gitlab.com/gitlab-com/Product/-/issues/2572)
1. [Multi opportunity assessment template](https://gitlab.com/gitlab-com/Product/-/issues/2306)
1. [PI Updates](https://gitlab.com/gitlab-com/Product/-/issues/2574)

## 2021-05-10
1. [Hiring Release PM](https://gitlab.com/gl-talent-acquisition/req-intake/-/issues/1375)
1. [Hiring Monitor PM](https://gitlab.com/gl-recruiting/req-intake/-/issues/1359)
1. [ADO Design Sprint](https://gitlab.com/gitlab-org/gitlab/-/issues/330163)
1. Ensure Data Dictionary work is scheduled for for [Release](https://gitlab.com/gitlab-org/gitlab/-/issues/321818)
1. [Release Planning](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/54)
1. Deployment based Optimization (Issue to be created)

## 2021-05-03
1. [CS Skills Exchange - Incident Management](https://gitlab.com/gitlab-com/sales-team/cs-skills-exchange/-/issues/127)
1. [Hiring Release PM](https://gitlab.com/gl-talent-acquisition/req-intake/-/issues/1375)
1. [Hiring Monitor PM](https://gitlab.com/gl-recruiting/req-intake/-/issues/1359)
1. Ensure Data Dictionary work is scheduled for for [Monitor](https://gitlab.com/gitlab-org/gitlab/-/issues/321812) and [Release](https://gitlab.com/gitlab-org/gitlab/-/issues/321818)
1. [Release Planning](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/54)
1. Monitor Release Posts
1. Review [GitLab Kubernetes Agent Themes Evaluation](https://docs.google.com/spreadsheets/d/1gf2rnOBahSR_-1IWjDwJYVIoN_EemU24oC55LHG4f_M/edit?ts=608a5f17#gid=0)

## 2021-04-19
1. [Hiring Monitor PM](https://gitlab.com/gl-recruiting/req-intake/-/issues/1359)
1. Hiring Release PM - Start by creating a job description
1. [Make changes to deployment directions based on feedback](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/79845)
1. Data Dictionary for [Monitor](https://gitlab.com/gitlab-org/gitlab/-/issues/321812) and [Release](https://gitlab.com/gitlab-org/gitlab/-/issues/321818)
1. Continue Handbook improvement for [wg_fy21_product_engagement](https://gitlab.slack.com/archives/C01P9FJRDHS)
1. Get vaccinated!!

## 2021-04-12
1. [Hiring Monitor PM](https://gitlab.com/gl-recruiting/req-intake/-/issues/1359)
1. [Follow-up on deployment directions](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11173)
1. Record video for deployment direction
1. Data Dictionary for [Monitor](https://gitlab.com/gitlab-org/gitlab/-/issues/321812) and [Release](https://gitlab.com/gitlab-org/gitlab/-/issues/321818)
1. [L&D for PM managers](https://gitlab.com/gitlab-com/Product/-/issues/2274)
1. Continue Handbook improvement for [wg_fy21_product_engagement](https://gitlab.slack.com/archives/C01P9FJRDHS)

## 2021-04-05
1. [Hiring Monitor PM](https://gitlab.com/gl-recruiting/req-intake/-/issues/1359)
1. [Follow-up on deployment directions](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11173) and [here](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/78546)
1. Record video for deployment direction
1. [Remove and simplify unnecessary handbook content for PMs](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/78907)
1. [L&D for PM managers](https://gitlab.com/gitlab-com/Product/-/issues/2274)
1. Handoff from Sarah for Monitor PM


## 2021-03-29
1. Prep for hiring
1. [Gigaom Analyst call](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/4809)
1. [Clarify PM job](https://gitlab.com/groups/gitlab-com/-/epics/1386)
1. [Deployment direction follow-up](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11173)

## 2021-03-15
1. [Deployment Delivery](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/76519)
1. [wg_fy21_product_engagement](https://gitlab.slack.com/archives/C01P9FJRDHS) working group
1. Get ready for a week off in [Sunriver](https://visitcentraloregon.com/sunriver/)

## 2021-03-08
1. Direction page for [Delivery](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/76519)
1. [PM Main Reponsibilities](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/76657)
1. Populate [APM SEG Pipeline](https://gitlab.com/gl-recruiting/req-intake/-/issues/1287)
1. Monitor Research - Need to create an issue still
1. Write Monitor Blog on problems - https://gitlab.com/gitlab-com/Product/-/issues/2048

## 2021-03-01
1. Create a basic financial model for self-hosting and partnership as part of https://gitlab.com/gitlab-com/Product/-/issues/2085
1. Connect with CS leaders - https://gitlab.com/gitlab-com/Product/-/issues/2210
1. Topology between Configure, Monitor, Release - https://gitlab.com/gitlab-com/Product/-/issues/2235
1. Start helping to [DeProcess the PM job](https://gitlab.com/gitlab-com/Product/-/issues/2055)
1. Explore the topic on [what makes an optimal product group](https://gitlab.com/gitlab-com/Product/-/issues/2236)

## 2021-02-22
1. Create a basic financial model for self-hosting and partnership as part of https://gitlab.com/gitlab-com/Product/-/issues/2085
1. Think through how [Monitor Release Configure can become land stages and why](https://gitlab.com/gitlab-com/Product/-/issues/2118) 
1. [Review/update Data dictionary](https://gitlab.com/gitlab-com/Product/-/issues/2113)

## 2021-02-08
1. Monitor - [APM categories business model](https://gitlab.com/gitlab-com/Product/-/issues/2085)
1. Update Monitor direction with [APM lessons learned and new strategy](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/74115)
1. [Improve Sisense Dashboards](https://gitlab.com/gitlab-com/Product/-/issues/2133) for Release and Configure and possibly include handbook resource for all Product
1. Think through how [Monitor Release Configure can become land stages and why](https://gitlab.com/gitlab-com/Product/-/issues/2118)

## 2021-02-01
1. Monitor - [APM categories business model](https://gitlab.com/gitlab-com/Product/-/issues/2085)
1. Deep dive into Configure, Release PIs - [PI reviews](https://gitlab.com/gitlab-com/Product/-/issues/2073)
1. [DIB certification](https://gitlab.com/gitlab-com/Product/-/issues/2082) 
1. Update Monitor Direction with lessons learned and go-forward strategy

## 2021-01-19
1. Re-kickoff SPG search for Monitor and be aligned with Product Leaders/Eng Leaders and Signed
1. Work on people development
1. Management and crucial conversation training

## 2020-11-23
1. Short week to wrap things up before [CEO Shadow](https://gitlab.com/gitlab-com/ceo-shadow/onboarding/-/issues/59) starting on 2020-11-30
1. Complete [Monitor strategy deck](https://docs.google.com/presentation/d/1Iw79oaSZg1OVAmubIhXQZOAsKd_snxKUXrLCjSsawzs/edit#slide=id.g29a70c6c35_0_68)

## 2020-10-19
1. Help wrap up any RPIs
1. [PM Coverage for Monitor Health](https://gitlab.com/gitlab-com/Product/-/issues/1627)
1. Review and update (as needed) Release/Monitor/Configure markets
1. Coffee chats with release members or related members
1. Roadmap APM categories for community contribution

## 2020-10-05
1. [Read and document feedback on Release and progressive delivery direction pages](https://gitlab.com/gitlab-com/Product/-/issues/1600)
1. Update [k8s GTM epic](https://gitlab.com/groups/gitlab-org/configure/-/epics/4)
1. Start connecting with Release team members
1. Roadmap APM categories for community contribution

## 2020-09-28
1. ✅ Complete [Snowplow template](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/43437) and other self service improvements for Snowplow
1. ... Dig into configure Indicators
1. ✅ Plan for 3 year mockup jam
1. ❌ Map out the 90s site idea

## 2020-09-21
1. ... Product Indicators - dive into the details of Configure
1. ✅ Review and edit K8s agent blog post
1. ✅ Slow down and learn
1. ✅ Create a writing plan

## 2020-09-14
1. ✅ PMs CDF update
1. ✅ Take a holistic look and update Monitor directions
1. ❌ Product Indicators - dive into the details of Configure

## 2020-09-08
1. ✅ [Coverage for Health](https://gitlab.com/gitlab-com/Product/-/issues/1500)
1. ✅ [Make progress on Health GMAU](https://gitlab.com/gitlab-org/gitlab/-/issues/233933)
1. ... Continue cleanup tasks after APM realignment

## 2020-08-17
1. [release posts manager activities](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/58559)
1. Create follow up issues to [Monitor GTM frictions](https://gitlab.com/groups/gitlab-com/-/epics/755)
1. Deep dive into PIs

## 2020-08-10
1. ✅ Review [release posts](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/58559) as Release Post Manager for 13.3
1. ✅ Merge [Buyer based tier competency](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/56525)
1. ❌ Create follow up issues to [Monitor GTM frictions](https://gitlab.com/groups/gitlab-com/-/epics/755)
1. ✅ Update Monitor direction with NR news

## 2020-08-03
1. ✅  Release Post Manager prep for 13.3
1. ... [Buyer based tier competency](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/56525)
1. ... Follow up with todos on [Monitor GTM frictions](https://gitlab.com/groups/gitlab-com/-/epics/755)
1. ✅  [Monitor JTBD documentation](https://gitlab.com/gitlab-org/gitlab/-/issues/216479)

## 2020-07-20
1. Release post manager shadow
1. Document Monitor GTM frictions
1. Continue push on [Monitor Direction updates](https://gitlab.com/gitlab-com/Product/-/issues/1069)
1. [Distributed Tracing Partnership](https://gitlab.com/gitlab-com/Product/-/issues/1307)
1. [Monitor JTBD documentation](https://gitlab.com/gitlab-org/gitlab/-/issues/216479)

## 2020-07-13
1. ✅  Configure planning for 13.3
1. ✅  Configure release posts
1. ✅  Release post manager shadow
1. ✅  Finish Serverless direction update as part of Configure direction updates on non-focus categories](https://gitlab.com/gitlab-com/Product/-/issues/1199)
1. ❌  Dig into sales partnership for MOnitor and Configure 

## 2020-07-06
1. ... 360 review meeting with Viktor and Dov
1. ... Dig into sales partnership for MOnitor and Configure
1. ❌  Distributed Tracing Partnership
1. ❌  Finish Serverless direction update as part of Configure direction updates on non-focus categories](https://gitlab.com/gitlab-com/Product/-/issues/1199)
1. ... Continue push on [Monitor Direction updates](https://gitlab.com/gitlab-com/Product/-/issues/1069)
1. ❌  [Monitor JTBD documentation](https://gitlab.com/gitlab-org/gitlab/-/issues/216479)

## 2020-06-29
1. ... 360 review meetings with Sarah and Viktor (Dov next week)
2. ❌  Define tracing partnership
3. ❌  Finish Serverless direction update as part of Configure direction updates on non-focus categories](https://gitlab.com/gitlab-com/Product/-/issues/1199)
4. ✅  Continue push on [Monitor Direction updates](https://gitlab.com/gitlab-com/Product/-/issues/1069)
5. ❌  [Monitor JTBD documentation](https://gitlab.com/gitlab-org/gitlab/-/issues/216479)

## 2020-06-22
1. ✅ Continue push on [Monitor Direction updates](https://gitlab.com/gitlab-com/Product/-/issues/1069)
1. ✅ Continue [Configure direction updates on non-focus categories](https://gitlab.com/gitlab-com/Product/-/issues/1199)
1. ❌ Define tracing partnership
1. ... [Monitor JTBD documentation](https://gitlab.com/gitlab-org/gitlab/-/issues/216479)

## 2020-06-15
1. ✅ Self review
2. ✅ Review release posts
3. ❌ Continue push on [Monitor Direction updates](https://gitlab.com/gitlab-com/Product/-/issues/1069)
4. ...Configure direction updates on non-focus categories

## 2020-06-08
1. ✅ 360 Feedback
2. ✅ Support OpsGenie planning
3. ❌ Continue push on [Monitor Direction updates](https://gitlab.com/gitlab-com/Product/-/issues/1069)
4. ❌ [GTM activities for Configure team](https://gitlab.com/gitlab-com/Product/-/issues/1207)


## 2020-06-01
1. ... 360 Feedback
2. ❌ Continue push on [Monitor Direction updates](https://gitlab.com/gitlab-com/Product/-/issues/1069)
3. ✅ [Who is the GitLab Metrics user?](https://gitlab.com/gitlab-org/gitlab/-/issues/217011)
4. ... [GTM activities for Configure team](https://gitlab.com/gitlab-com/Product/-/issues/1207)

## 2020-05-26
1. ✅ [Walkthrough Dispatch](https://gitlab.com/gitlab-com/Product/-/issues/1194)
2. ✅ Continue push on [Monitor Direction updates](https://gitlab.com/gitlab-com/Product/-/issues/1069)
3. ❌ Product Indicators
4. ... [Who is the GitLab Metrics user?](https://gitlab.com/gitlab-org/gitlab/-/issues/217011)

## 2020-05-18
1. ... Make progress on improving the Monitor Direction Page](https://gitlab.com/gitlab-com/Product/-/issues/1069)
2. ✅ Push forward a decision in [product analytics discussion](https://gitlab.com/gitlab-com/Product/-/issues/1162)

## 2020-05-11
1. ✅ Webinar with LightStep on May 12
2. ✅ Document product analytics direction in an issue
3. ✅ Taking PTO 05-13 to 05-15

## 2020-05-04
1. ✅ Prepare for [LightStep Webinar](https://gitlab.com/gitlab-com/Product/-/issues/827)
1. ✅ Add quarterly goals to direction pages - https://gitlab.com/gitlab-com/Product/-/issues/1015 
1. ... Improve Monitor Direction Page](https://gitlab.com/gitlab-com/Product/-/issues/1069)
1. ... Do research and recommend next steps for product analytics and Monitor's involvement. - https://gitlab.com/gitlab-org/gitlab/-/merge_requests/27730#note_334027798

## Week of 2020-04-27

1. ✅ Configure Design Sprint
2. ✅ [Q2 Goals](https://gitlab.com/gitlab-com/Product/-/issues/1015)
3. ✅ [Group conversation prep](https://gitlab.com/gitlab-com/Product/-/issues/1108#note_332091542)
4. ❌ [Improve Monitor Direction Page](https://gitlab.com/gitlab-com/Product/-/issues/1069)

## Week of 2020-04-20

1. ✅ [Title, abstract, plan for Webinar](https://gitlab.com/gitlab-com/marketing/product-marketing/-/issues/2398#note_326405329) with Austin from LightStep
1. ❌ [Familiarize myself with Sisense and how to query gitlab PG Db, snowplow event data for .com and how to use usage ping data for all instances](https://gitlab.com/gitlab-com/Product/-/issues/1068)
1. ... [Q2 Goals](https://gitlab.com/gitlab-com/Product/-/issues/1015)
1. ... [Improve Monitor Direction Page](https://gitlab.com/gitlab-com/Product/-/issues/1069)

## Week of 2020-04-13
### Priorities
1. ✅ OpenTelemetry 101
2. ❌ Familiarize myself with Sisense and how to query gitlab PG Db, snowplow event data for .com and how to use usage ping data for all instances
3. ✅ Finish ADO/alert/incident walkthrough
4. ❌Start improvement on Monitor direciton page
5. ✅ Read and familiarize all of configure direction pages and current capabilities.

## Week of 2020-04-06

### Accomplishments
1. Made progress for North Star Metrics and have a plan on what we should measure
2. Met all the engineers/ux for configure
3. Closed Monitor to Observability renaming MR
4. Started a walkthrough - wasn't quite able to finish because pipeline jobs were super slow :(

### Priorities
1. Create plan for [North Star Metrics](https://gitlab.com/gitlab-com/Product/-/issues/885)
2. Meet rest of Configure team
3. Make progress on Observability renaming [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/43827)
4. Finally start and do ADO walkthrough

## Week of 2020-03-30

### Accomplishments
1. Had a productive week working a staggered schedule with no family getting sick or injured!
2. Made progress for North Star Metrics - see [worksheet](https://docs.google.com/spreadsheets/d/10TJBsDzmB5ldQ4rXcMvNyLO_jkVRBqCEI5vtqVihfmk/edit#gid=0)
3. Met a bunch of configure teammates
4. Started continuous interviewing course and almost finished intro to kubernetes course
5. Category improvements

### Focus areas
1. Make progress on North Star Metrics
1. Meet Configure teammates
1. Learn and do my first walkthorough!
1. Category improvements

## Week of 2020-03-23

### Accomplishments
1. Survived (barely) a week of intense young children at home and hurting themselves in the process
2. Learned a lot about Kubernetes (ramping up from 0)

### Focus areas

1. Open up Monitor renaming MR to an wider audience
2. Make progress on North Star Metrics
3. Meet Configure teammates
4. Learn and do my first walkthorough!

## Week of 2020-03-16

### Accomplishments

1. Read a few configuration direction pages
2. Made a bunch of MRs updating the Monitor Section [here](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/43827), [here](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/44072), [here](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/44102)
3. Signed up and started learning Kubernetes with EdX
4. Draft for incident management blog complete - need to [publish it](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/6807#note_309663146)
5. Started getting into a routine by end of week with kids at home!

### Focus areas

1. Support Dov and learn to be effective with a modified working schedules with kids at home during the COVID-19 forced school closure.
2. Learn the GitLab product. Read configure direction pages. A tangible output of this effort is product walkthroughs.
3. Meet new teammates - schedule coffee chats with configure team members and other Ops PMs.
