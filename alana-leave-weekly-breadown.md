## 2022-04-01
* Updated docs to reflect removal of [metrics and self-monitoring will be 16.0](https://gitlab.com/gitlab-org/gitlab/-/issues/356728)
* Adjust 14.10 plan accordingly based on above
* Start documenting overall incident management workflow in mural
* Nicole G internship started!
* [Update Continuous Verification JTBD](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/101730)
