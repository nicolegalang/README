---
layout: markdown_page
title: "Kevin Chu's README"
description: "Kevin is a Product Manager at GitLab. This page is a little about him.
---

## Kevin's README

Hello! I am Kevin and I'm a [Group Product Manager](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/job-families/product/group-manager-product/index.html.md) in the [Ops Section](https://about.gitlab.com/direction/ops/). I am directly involved in [Release](https://about.gitlab.com/direction/release/), [Configure](https://about.gitlab.com/direction/configure/), and [Monitor](https://about.gitlab.com/direction/monitor/). This README is meant to tell you a bit more about myself and to provide a glimpse to what it might be like to work with me. Please feel free to contribute to this page by opening a merge request.

* [GitLab Handle](https://gitlab.com/kbychu)
* [Team Page](/company/team/#kbychu)

## About me

* I grew up in Taiwan and moved to Seattle, WA, USA when I was 10 with my family. I spent the past decade living in London, Madrid, and Washington DC and loved it!
* I now live in Portland, OR with my wife, 3 little kids, and a [dog](company/team-pets/#279-chopito).
* I love to laugh and am not too serious in general.
* I am excitable.
* I cook a lot.
* I can struggle with starting too many things at once - I'd like to learn to do less and focus more.
* Despite being generally opinionated, I love changing my mind when I learn something more.
* I enjoy learning about new things and find great satisfaction in making connections between seemingly disparate fields of study.
* The particular GitLab values that sing to me are [iteration](https://about.gitlab.com/handbook/values/#iteration) and [results](https://about.gitlab.com/handbook/values/#results).
* I can recall random details or facts from past interactions. 
* I started my career as a developer, before starting a company and moving to product management.
* I attempt to start my work day early at around 6am PST. I try to finish my work day between 3-5pm PST depending on the day.

## Communicating and working with me

* I value making decisions quickly. I believe having a decision is always better than having no decision because we can always iterate or reverse course for two-way door decisions.
* I believe is is ok to figure things out along the way. In other words, I value making mistakes and learning from them. I practice this and it may seem like I don't know what I am doing. Please be patient when you observe this or ask me about it.
* I strive to be a good listener, but may often start reacting too quickly. Please feel free to call me out on it.
* I use the design thinking process (or a subset of it) in most things I do - emphathize, problem definition, ideate (divergent then convergent thinking, brainstorming), prototype, and test. This style of working is not congruent with all styles of working as it can be perceived as challenging or constantly rehashing previous decisions.
* I am quick to apologize and own mistakes when they happen.
* I believe that having independent, autonomous teams is the best, most efficient way to ship products that matter. 
* I am not good at providing negative feedback and will need help to get better at it.
* I am not good at giving specific instructions. I need help identifying when I need to be direct, please point them out to me.
* I enjoy building deep, personal relationships with the people I work with. If this is not your preferred style of interaction, please feel free to let me know.
* To find out what I am working on, check out my [priorities](priorities.md) or check out my [.com personal board](https://gitlab.com/groups/gitlab-com/-/boards/1511290?assignee_username=kbychu&) and [.org personal board](https://gitlab.com/groups/gitlab-org/-/boards/1567586?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=kbychu)
* Also what I am working on, though at a slightly different cadence see my [improvement plan](improvement_plan.md)

## What I am working on to improve

* Communication - Be cognizant of when to use low-context or high-context communication.
* Learn about and be better plugged-in to the massive space/opportunity in [deployment](https://about.gitlab.com/direction/deployment/)
* I want to build my expertise to coach others to run ultra-efficient and effective Async/Remote Product Management teams.
